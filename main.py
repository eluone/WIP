#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 04/04/21
# Modified: 12/12/21

import wx
import wx.grid
import wx.adv
import wx.lib.scrolledpanel
import json

from pathlib import Path

from common.config import load_config
from common.api import load_market_cache, save_market_cache, api_check
from common.gui_bits import on_about

from gui.blueprint import BlueprintDialog
from gui.cache_browser import CacheBrowser
from gui.configure import ConfigWindow

from common.init_common import gen_mat_dict, gen_bpo_dict
from common.functions import get_matches, sort_for_output, add_market_data, unfiltered_bpo_list, apply_user_filters
from common.log_setup import setup_logger

import locale
locale.setlocale(locale.LC_ALL, '')

# Setup log destination and formatter
log_dir = Path.cwd()
logger = setup_logger(log_dir, __name__)


class MainWindow(wx.Frame):
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((900, 600))
        self.SetTitle("Eve Online Salvage Solver")
        self.SetBackgroundColour(wx.NullColour)  # Use system default colour

        # Load up saved or default config
        self.config = load_config()

        # Setup materials dictionary and material list
        self.mat_name_to_id_dict = gen_mat_dict(self.config)  # Only used in input list setup now
        self.bpo_dict = gen_bpo_dict(self.config)  # bpo_name_to_id_dict for Text to ID conversions

        # Let's try to load up previous material selections from the cache file.
        self.material_cache_json = Path('material.json')
        if self.material_cache_json.exists():
            with open(self.material_cache_json, 'r') as j:
                self.mat_qty_dict = json.load(j)  # Dictionary of material quantities with material names as keys
        else:
            self.mat_qty_dict = {}

        # Font Setup
        status_bar_font = wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, "Sans")
        main_font = wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, "Sans")

        # Menu Bar
        self.frame_menu_bar = wx.MenuBar()
        self.fileMenu = wx.Menu()
        self.menuAbout = wx.MenuItem(self.fileMenu, wx.ID_ABOUT, "&About", "", wx.ITEM_NORMAL)
        self.fileMenu.Append(self.menuAbout)
        self.menuCacheBrowser = wx.MenuItem(self.fileMenu, wx.ID_ANY, "Cache Browser", "", wx.ITEM_NORMAL)
        self.fileMenu.Append(self.menuCacheBrowser)
        self.menuConfig = wx.MenuItem(self.fileMenu, wx.ID_PREFERENCES, "&Configure", "", wx.ITEM_NORMAL)
        self.fileMenu.Append(self.menuConfig)
        self.menuExit = wx.MenuItem(self.fileMenu, wx.ID_EXIT, "E&xit", "", wx.ITEM_NORMAL)
        self.fileMenu.Append(self.menuExit)
        self.frame_menu_bar.Append(self.fileMenu, "File")
        self.SetMenuBar(self.frame_menu_bar)

        self.Bind(wx.EVT_MENU, on_about, self.menuAbout)
        self.Bind(wx.EVT_MENU, self.on_cache_browser, self.menuCacheBrowser)
        self.Bind(wx.EVT_MENU, self.on_config, self.menuConfig)
        self.Bind(wx.EVT_MENU, self.on_exit, self.menuExit)
        # Menu Bar end

        self.status_bar = self.CreateStatusBar(1, 0)
        self.status_bar.SetFont(status_bar_font)
        self.status_bar.SetStatusText('Welcome to E.O.S.S - %s - Idle' % self.config.version)

        self.main_panel = wx.Panel(self, wx.ID_ANY)
        main_rows_sizer = wx.BoxSizer(wx.VERTICAL)

        self.main_notebook = wx.Notebook(self.main_panel, wx.ID_ANY)
        main_rows_sizer.Add(self.main_notebook, 1, wx.EXPAND, 0)

        self.materials_pane = wx.ScrolledWindow(self.main_notebook, wx.ID_ANY, style=wx.TAB_TRAVERSAL)
        self.materials_pane.SetScrollRate(10, 10)
        self.main_notebook.AddPage(self.materials_pane, "Materials")

        # Setup Materials UI Tab
        material_sizer = wx.BoxSizer(wx.HORIZONTAL)
        # Use matDict dictionary to calculate input grid rows (+1 row for column headers)
        grid_num_rows = (len(self.mat_name_to_id_dict) + 1)
        input_grid_sizer = wx.FlexGridSizer(grid_num_rows, 4, 2, 2)

        header_1 = wx.StaticText(self.materials_pane, wx.ID_ANY, "Material")
        header_1.SetFont(main_font)
        input_grid_sizer.Add(header_1, 0, wx.ALL, 2)

        header_2 = wx.StaticText(self.materials_pane, wx.ID_ANY, "Qty")
        header_2.SetFont(main_font)
        input_grid_sizer.Add(header_2, 0, wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 2)

        header_3 = wx.StaticText(self.materials_pane, wx.ID_ANY, "ISK / Unit (Sell)")
        header_3.SetFont(main_font)
        input_grid_sizer.Add(header_3, 0, wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 2)

        header_4 = wx.StaticText(self.materials_pane, wx.ID_ANY, "ISK Total (Sell)")
        header_4.SetFont(main_font)
        input_grid_sizer.Add(header_4, 0, wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 2)

        self.mat_spinners = []  # Keep a list of the dynamically created spin_ctrl(s)
        self.mat_value_labels = []  # Keep a list of the material value labels
        self.mat_total_labels = []  # Keep a list of the material value total labels
        # Use matDict dictionary to generate input grid
        for key, value in self.mat_name_to_id_dict.items():
            # Update the inputGrid with the selected Materials from the database: (row, col, string)
            label = wx.StaticText(self.materials_pane, wx.ID_ANY, str(key))
            label.SetFont(main_font)
            input_grid_sizer.Add(label, 0, wx.ALIGN_CENTRE_VERTICAL, 0)

            if str(value) in self.mat_qty_dict:
                self.spin_ctrl = wx.SpinCtrl(self.materials_pane, wx.ID_ANY, str(self.mat_qty_dict[str(value)]),
                                             min=0, max=16777216,
                                             name=str(value), style=wx.ALIGN_CENTRE_HORIZONTAL)
            else:
                self.spin_ctrl = wx.SpinCtrl(self.materials_pane, wx.ID_ANY, "0",
                                             min=0, max=16777216,
                                             name=str(value), style=wx.ALIGN_CENTRE_HORIZONTAL)
            self.spin_ctrl.SetFont(main_font)
            input_grid_sizer.Add(self.spin_ctrl, 0, wx.ALL, 2)
            self.mat_spinners.append(self.spin_ctrl)

            self.value = wx.StaticText(self.materials_pane, wx.ID_ANY, label="", name=str(value))
            self.value.SetFont(main_font)
            input_grid_sizer.Add(self.value, 0, wx.ALIGN_CENTRE_VERTICAL | wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 2)
            self.mat_value_labels.append(self.value)

            self.total = wx.StaticText(self.materials_pane, wx.ID_ANY, label="", name=str(value))
            self.total.SetFont(main_font)
            input_grid_sizer.Add(self.total, 0, wx.ALIGN_CENTRE_VERTICAL | wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 2)
            self.mat_total_labels.append(self.total)

        input_grid_sizer.AddGrowableCol(0, 1)
        input_grid_sizer.AddGrowableCol(2, 1)
        input_grid_sizer.AddGrowableCol(3, 1)

        material_sizer.Add(input_grid_sizer, proportion=2, flag=wx.ALL | wx.EXPAND, border=15)

        # Setup Blueprints UI Tab
        self.blueprints_pane = wx.ScrolledWindow(self.main_notebook, wx.ID_ANY, style=wx.TAB_TRAVERSAL)
        self.blueprints_pane.SetScrollRate(10, 10)
        self.main_notebook.AddPage(self.blueprints_pane, "Blueprints")
        blueprint_sizer = wx.BoxSizer(wx.VERTICAL)

        self.outputList = wx.ListCtrl(self.blueprints_pane, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.outputList.SetFont(main_font)
        self.outputList.AppendColumn("Blueprint", format=wx.LIST_FORMAT_LEFT, width=350)
        self.outputList.AppendColumn("Mat Types", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.outputList.AppendColumn("Mat Qty", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.outputList.AppendColumn("Runs", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.outputList.AppendColumn("ISK / Unit", format=wx.LIST_FORMAT_RIGHT, width=110)
        self.outputList.AppendColumn("Total", format=wx.LIST_FORMAT_RIGHT, width=110)
        blueprint_sizer.Add(self.outputList, 1, wx.ALL | wx.EXPAND, 3)

        # UI Bottom Elements
        config_sizer = wx.BoxSizer(wx.HORIZONTAL)

        config_sizer.Add((20, 20), 1, wx.EXPAND, 0)  # Spacer element

        materials_missing_label = wx.StaticText(self.main_panel, wx.ID_ANY, "Missing Materials: ", style=wx.ALIGN_RIGHT)
        materials_missing_label.SetFont(main_font)
        materials_missing_label.SetMinSize((130, -1))
        config_sizer.Add(materials_missing_label, 0, wx.ALIGN_CENTRE_VERTICAL, 0)

        self.materials_missing_spin_ctrl = wx.SpinCtrl(self.main_panel, wx.ID_ANY, str(self.config.missing_mats),
                                                       min=0, max=65536,
                                                       name="materials_missing", style=wx.ALIGN_CENTRE_HORIZONTAL)
        self.materials_missing_spin_ctrl.SetFont(main_font)
        self.materials_missing_spin_ctrl.SetMinSize((125, -1))
        config_sizer.Add(self.materials_missing_spin_ctrl, 0, 0, 0)

        min_runs_label = wx.StaticText(self.main_panel, wx.ID_ANY, "Minimum Runs: ", style=wx.ALIGN_RIGHT)
        min_runs_label.SetFont(main_font)
        min_runs_label.SetMinSize((130, -1))
        config_sizer.Add(min_runs_label, 0, wx.ALIGN_CENTRE_VERTICAL, 0)
        self.min_runs_spin_ctrl = wx.SpinCtrl(self.main_panel, wx.ID_ANY, str(self.config.min_runs),
                                              min=0, max=65536,
                                              name="min_runs", style=wx.ALIGN_CENTRE_HORIZONTAL)
        self.min_runs_spin_ctrl.SetFont(main_font)
        self.min_runs_spin_ctrl.SetMinSize((125, -1))
        config_sizer.Add(self.min_runs_spin_ctrl, 0, 0, 0)

        config_sizer.Add((20, 20), 1, wx.EXPAND, 0)  # Spacer element

        self.override_checkbox = wx.CheckBox(self.main_panel, wx.ID_ANY, "Auto Filters")
        config_sizer.Add(self.override_checkbox, 0, wx.ALIGN_CENTRE_VERTICAL, 0)

        config_sizer.Add((20, 20), 1, wx.EXPAND, 0)  # Spacer element

        self.checkButton = wx.Button(self.main_panel, wx.ID_ANY, "Check")
        config_sizer.Add(self.checkButton, 0, 0, 0)
        main_rows_sizer.Add(config_sizer, 0, wx.ALL | wx.EXPAND, 3)

        self.materials_pane.SetSizer(material_sizer)
        self.blueprints_pane.SetSizer(blueprint_sizer)
        self.main_panel.SetSizer(main_rows_sizer)

        self.Layout()

        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_act, self.outputList)
        self.Bind(wx.EVT_SPINCTRL, self.check_missing_mats_value, self.materials_missing_spin_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.check_min_runs_value, self.min_runs_spin_ctrl)
        self.Bind(wx.EVT_BUTTON, self.on_check, self.checkButton)

        # Lets try to load up previous market data from file.
        self.market_data = load_market_cache()
        self.sorted_out_list = []

        logger.info(('E.O.S.S version: %s' % self.config.version))
        logger.debug(('Using SDE file %s' % self.config.sql_file))

    def on_act(self, event):  # Item double clicked in outputList
        logger.info("Running on_act...")

        if len(self.sorted_out_list) > 0:
            dlg = BlueprintDialog(None)
            # Pass selection and inputs to dialog
            list_index = event.GetIndex()
            bpo_id = self.sorted_out_list[list_index][0]  # This is the BPO id from the out_list list

            dlg.show(self.config, bpo_id, self.mat_qty_dict, self.bpo_dict, self.market_data)
            dlg.Destroy()  # finally destroy it when finished

        logger.info('on_act Complete...')

    def check_missing_mats_value(self, event):  # Value changed in missing_materials_spin_ctrl
        if int(self.materials_missing_spin_ctrl.GetValue()) > 0:
            self.min_runs_spin_ctrl.SetValue(0)

    def check_min_runs_value(self, event):  # Value changed in min_runs_spin_ctrl
        if int(self.min_runs_spin_ctrl.GetValue()) > 0:
            self.materials_missing_spin_ctrl.SetValue(0)

    def on_check(self, event):
        logger.info("Running on_check...")
        mat_id_list = []  # mat_id_list will be a list of material typeIDs to be passed to SQL query
        self.mat_qty_dict.clear()  # Reset Dictionary of material quantities with material names as keys

        for i in self.mat_spinners:  # Use list of material spin_ctrl(s) to query all from grid
            # Collect a price for all materials as we might need prices for missing materials
            if i.GetValue() > 0:
                mat_id_list.append(int(i.GetName()))  # Use dict to convert material name to id
                # Get typeID from matDict
                self.mat_qty_dict[int(i.GetName())] = i.GetValue()  # Store values of each spin_ctrl greater than 0

        self.market_data.update(api_check(self.config, mat_id_list, self.market_data))

        for j in self.mat_value_labels:
            mat_id = int(j.GetName())
            if mat_id in self.market_data:
                j.SetLabel(locale.format_string("%0.2f", self.market_data[mat_id].sell, grouping=True))

        for k in self.mat_total_labels:
            mat_id = int(k.GetName())
            if mat_id in self.mat_qty_dict:
                if mat_id in self.market_data:
                    total = self.mat_qty_dict[mat_id] * self.market_data[mat_id].sell
                    k.SetLabel(locale.format_string("%0.2f", total, grouping=True))

        logger.debug(('mat_id_list: %s' % mat_id_list))
        logger.debug(('mat_qty_dict: %s' % self.mat_qty_dict))

        if mat_id_list:
            # Save material selections
            # This will create the file if it doesn't exist
            with open(self.material_cache_json, 'w') as mj:
                json.dump(self.mat_qty_dict, mj, indent=4)

            # Get all Blueprints that have listed Materials
            # match_list: Counter Object with Blueprint ID/Material Name as keys and matches as value
            # ({Material Name / BPO ID : Matches]...}
            match_list = get_matches(self.config, mat_id_list)

            # Get max missing material types from UI element
            num_missing_mats = int(self.materials_missing_spin_ctrl.GetValue())

            # Get minimum runs from UI element
            min_runs = int(self.min_runs_spin_ctrl.GetValue())

            # Use UI element rather than hardcoded config.allow_config_override
            if self.override_checkbox.GetValue():
                allow_config_override = "Y"
            else:
                allow_config_override = "N"

            # Generate initial list (of lists) of matches and details
            # full_list: [[0] BPO ID, [1] Material Type Matches, [2] Material Qty Matches, [3] Max Possible Runs]...
            full_list = unfiltered_bpo_list(self.config, match_list, self.mat_qty_dict, self.bpo_dict)

            # Filter list by minimum runs (min_runs) or material types missing (num_missing_mats)
            # out_list_data: [[0] BPO ID, [1] Material Type Matches, [2] Material Qty Matches, [3] Max Possible Runs]...
            out_list_data = apply_user_filters(full_list, self.bpo_dict, min_runs,
                                               num_missing_mats, allow_config_override)

            if min_runs > 0:
                status_text = ("BPOs using Materials: %i - Filtered: %i - Target Minimum Runs: %i - Market: %s" %
                               (len(full_list), len(out_list_data), min_runs, self.config.market_name.title()))
            else:
                status_text = ("BPOs using Materials: %i - Filtered: %i - Missing Material Types: %i - Market: %s" %
                               (len(full_list), len(out_list_data), num_missing_mats, self.config.market_name.title()))

            self.status_bar.SetStatusText(status_text)

            self.outputList.DeleteAllItems()  # Clear the outputList to start with

            # Only add data to the outputList ctrl if we have something
            if len(out_list_data) > 0:
                # Add the id of each Blueprint product to a list to be checked against the API
                market_check_list = []
                for item in out_list_data:
                    if self.bpo_dict[item[0]].output_type_id not in self.market_data:
                        market_check_list.append(self.bpo_dict[item[0]].output_type_id)
                if len(market_check_list) > 0:
                    self.market_data.update(api_check(self.config, market_check_list, self.market_data))

                # This will add [4] Unit Price, [5] Total Price
                text_for_output = add_market_data(out_list_data, self.bpo_dict, self.market_data)

                # For testing loop here to add BPO name as [6]
                for item in text_for_output:
                    # Add BPO name to each item in the out_list_data so that we can sort on name
                    item.append(str(self.bpo_dict[item[0]].type_name))

                # Sort the data on config.sort_column number
                self.sorted_out_list = sort_for_output(text_for_output, self.config.sort_column)

                # Use data for rows in outputList (convert all to strings)
                for row in self.sorted_out_list:
                    # Just before output change the Blueprint ID to its name
                    # bpo_name = str(self.bpo_dict[row[0]].type_name)
                    req_mats = self.bpo_dict[row[0]].num_materials_req  # Get number of material types required for BPO

                    if row[4] > 0:
                        unit_price = locale.format_string("%0.2f", row[4], grouping=True)
                        if row[3] > 0:
                            total_price = locale.format_string("%0.2f", row[5], grouping=True)
                        else:
                            total_price = "N/A"
                    else:
                        unit_price = "No Data"
                        total_price = "N/A"

                    self.outputList.Append((row[6],
                                            ("%s / %s" % (row[1], req_mats)),
                                            ("%s / %s" % (row[2], req_mats)),
                                            str(row[3]),
                                            unit_price,
                                            total_price))
            else:
                # We've got no results stored in out_list_data
                self.outputList.Append(("Filters Returned No Results - Try Auto Filters Option", "", "", "", ""))

            # Save the updated market_data to file
            save_market_cache(self.market_data)

        # Swap to the "Blueprint" tab to show results
        self.main_notebook.ChangeSelection(1)
        logger.info('on_check Complete...')

    def on_config(self, event):
        dlg = ConfigWindow(None)
        dlg.show(self.config)
        dlg.Destroy()  # finally destroy it when finished
        self.config = load_config()

    def on_cache_browser(self, event):  # May move to inside config dialog
        dlg = CacheBrowser(None)
        # Pass selection and inputs to dialog
        dlg.show(self.market_data)
        dlg.Destroy()  # finally destroy it when finished

    def on_exit(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure to quit?', 'Please Confirm',
                               wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            self.Close(True)


if __name__ == "__main__":
    app = wx.App()
    main = MainWindow(None, wx.ID_ANY, "")
    app.SetTopWindow(main)
    main.Show()
    app.MainLoop()
