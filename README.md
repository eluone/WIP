**Eve Online Salvage Solver**

_Work In Progress_

Idea:
Given a list of raw materials (salvage items) produce a list of possible items that could be manufactured in the game 'Eve Online' by CCP Games.

Flow:
- Input List of materials
- Query SQLite db for blueprints that use materials above
- Query Evepraisal.com API for in game market data
- Output list of blueprints with most requirements met

Python modules required:
- wxPython, urllib3, certifi, json, sqlite3

Future:
- GUI Rework
