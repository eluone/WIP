#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 31/07/21
# Modified: 11/12/21

import wx

from common.config import save_config


class ConfigWindow(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: ConfigWindow.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.SetTitle("Configurations")

        main_sizer = wx.BoxSizer(wx.VERTICAL)

        options_grid_sizer = wx.GridSizer(8, 2, 2, 2)
        main_sizer.Add(options_grid_sizer, 1, wx.EXPAND | wx.ALL, 4)

        label_1 = wx.StaticText(self, wx.ID_ANY, "Missing Materials Default")
        options_grid_sizer.Add(label_1, 0, 0, 0)

        self.missing_mats_spin_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "2", min=0, max=100)
        options_grid_sizer.Add(self.missing_mats_spin_ctrl, 0, 0, 0)

        label_2 = wx.StaticText(self, wx.ID_ANY, "Minimum Runs Default")
        options_grid_sizer.Add(label_2, 0, 0, 0)

        self.min_runs_spin_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=100)
        options_grid_sizer.Add(self.min_runs_spin_ctrl, 0, 0, 0)

        label_3 = wx.StaticText(self, wx.ID_ANY, "Auto Filters Default")
        options_grid_sizer.Add(label_3, 0, 0, 0)

        self.allow_override_choice = wx.Choice(self, wx.ID_ANY, choices=["No", "Yes"])
        options_grid_sizer.Add(self.allow_override_choice, 0, 0, 0)

        label_4 = wx.StaticText(self, wx.ID_ANY, "Default Sort Column")
        options_grid_sizer.Add(label_4, 0, 0, 0)

        self.sort_choice = wx.Choice(self, wx.ID_ANY, choices=["Blueprint Name", "Max Runs", "ISK / Unit", "ISK Total"])
        options_grid_sizer.Add(self.sort_choice, 0, 0, 0)

        label_5 = wx.StaticText(self, wx.ID_ANY, "Market")
        options_grid_sizer.Add(label_5, 0, 0, 0)

        self.market_choice = wx.Choice(self, wx.ID_ANY, choices=["Amarr", "Dodixie", "Hek", "Jita", "Perimeter", "Rens"])
        options_grid_sizer.Add(self.market_choice, 0, 0, 0)

        label_6 = wx.StaticText(self, wx.ID_ANY, "Buy Data")
        options_grid_sizer.Add(label_6, 0, 0, 0)

        self.buy_choice = wx.Choice(self, wx.ID_ANY, choices=["avg", "max", "median", "min", "percentile"])
        options_grid_sizer.Add(self.buy_choice, 0, 0, 0)

        label_7 = wx.StaticText(self, wx.ID_ANY, "Sell Data")
        options_grid_sizer.Add(label_7, 0, 0, 0)

        self.sell_choice = wx.Choice(self, wx.ID_ANY, choices=["avg", "max", "median", "min", "percentile"])
        options_grid_sizer.Add(self.sell_choice, 0, 0, 0)

        label_8 = wx.StaticText(self, wx.ID_ANY, "Cache Max Age (Seconds)")
        options_grid_sizer.Add(label_8, 0, 0, 0)

        self.age_spin_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "1800", min=60, max=86400)
        options_grid_sizer.Add(self.age_spin_ctrl, 0, 0, 0)

        button_sizer = wx.StdDialogButtonSizer()
        main_sizer.Add(button_sizer, 0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.button_CANCEL = wx.Button(self, wx.ID_CANCEL, "")
        button_sizer.AddButton(self.button_CANCEL)

        self.button_APPLY = wx.Button(self, wx.ID_APPLY, "")
        button_sizer.AddButton(self.button_APPLY)

        self.button_OK = wx.Button(self, wx.ID_OK, "")
        self.button_OK.SetDefault()
        button_sizer.AddButton(self.button_OK)

        button_sizer.Realize()

        self.SetSizer(main_sizer)
        main_sizer.Fit(self)

        self.SetEscapeId(self.button_CANCEL.GetId())
        self.SetAffirmativeId(self.button_OK.GetId())

        self.Layout()

        self.Bind(wx.EVT_SPINCTRL, self.check_missing_mats_value, self.missing_mats_spin_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.check_min_runs_value, self.min_runs_spin_ctrl)
        self.Bind(wx.EVT_BUTTON, self.save, self.button_APPLY)

        self.config = []

    def show(self, config):
        self.config = config

        if config.missing_mats >= 0:
            self.missing_mats_spin_ctrl.SetValue(config.missing_mats)
        else:
            self.missing_mats_spin_ctrl.SetValue(0)

        if config.min_runs >= 0:
            self.min_runs_spin_ctrl.SetValue(config.min_runs)
        else:
            self.min_runs_spin_ctrl.SetValue(1)

        override_dict = {"N": 0, "Y": 1}
        if config.allow_config_override in override_dict:
            self.allow_override_choice.SetSelection(override_dict[config.allow_config_override])
        else:
            self.allow_override_choice.SetSelection(1)

        # Output List Sorting Columns: 6: Blueprint Name(0), 3: Max Runs(1), 4: ISK / Unit(2), 5: ISK Total(3)
        sort_dict = {6: 0, 3: 1, 4: 2, 5: 3}
        if config.sort_column in sort_dict:
            self.sort_choice.SetSelection(sort_dict[config.sort_column])
        else:
            self.sort_choice.SetSelection(0)

        market_dict = {"amarr": 0, "dodixie": 1, "hek": 2, "jita": 3, "perimeter": 4, "rens": 5}
        if config.market_name in market_dict:
            self.market_choice.SetSelection(market_dict[config.market_name])
        else:
            self.market_choice.SetSelection(1)

        # Used for buy and sell conversion
        field_dict = {"avg": 0, "max": 1, "median": 2, "min": 3, "percentile": 4}
        if config.buy_data_field in field_dict:
            self.buy_choice.SetSelection(field_dict[config.buy_data_field])
        else:
            self.buy_choice.SetSelection(0)

        if config.sell_data_field in field_dict:
            self.sell_choice.SetSelection(field_dict[config.sell_data_field])
        else:
            self.sell_choice.SetSelection(0)

        if config.cache_max_age > 0:
            self.age_spin_ctrl.SetValue(config.cache_max_age)
        else:
            self.age_spin_ctrl.SetValue("3200")

        self.ShowModal()

    def check_missing_mats_value(self, event):  # Value changed in missing_materials_spin_ctrl
        if int(self.missing_mats_spin_ctrl.GetValue()) > 0:
            self.min_runs_spin_ctrl.SetValue(0)

    def check_min_runs_value(self, event):  # Value changed in min_runs_spin_ctrl
        if int(self.min_runs_spin_ctrl.GetValue()) > 0:
            self.missing_mats_spin_ctrl.SetValue(0)

    def save(self, event):
        # Update default config and save
        # Use opposite mappings to read data from UI elements

        override_dict = {0: "N", 1: "Y"}

        # Output List Sorting Columns: 6: Blueprint Name(0), 3: Max Runs(1), 4: ISK / Unit(2), 5: ISK Total(3)
        sort_dict = {0: 6, 1: 3, 2: 4, 3: 5}

        market_dict = {0: "amarr", 1: "dodixie", 2: "hek", 3: "jita", 4: "perimeter", 5: "rens"}

        # Used for buy and sell conversion
        field_dict = {0: "avg", 1: "max", 2: "median", 3: "min", 4: "percentile"}

        missing_mats = int(self.missing_mats_spin_ctrl.GetValue())
        min_runs = int(self.min_runs_spin_ctrl.GetValue())
        allow_config_override = str(override_dict[self.allow_override_choice.GetSelection()])
        sort_column = int(sort_dict[self.sort_choice.GetSelection()])
        market_name = str(market_dict[self.market_choice.GetSelection()])
        buy_data_field = str(field_dict[self.buy_choice.GetSelection()])
        sell_data_field = str(field_dict[self.sell_choice.GetSelection()])

        # Cache age
        cache_max_age = self.age_spin_ctrl.GetValue()

        self.config.missing_mats = missing_mats
        self.config.min_runs = min_runs
        self.config.allow_config_override = allow_config_override
        self.config.sort_column = sort_column
        self.config.market_name = market_name
        self.config.buy_data_field = buy_data_field
        self.config.sell_data_field = sell_data_field
        self.config.cache_max_age = cache_max_age

        save_config(self.config)
