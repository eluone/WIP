#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 02/04/21
# Modified: 05/09/21

import wx
import wx.adv

from common.config import load_config


def on_error(error):
    dlg = wx.MessageDialog(None, 'An error has occurred:\n' + error, '', wx.OK | wx.ICON_ERROR)
    dlg.ShowModal()  # Show it
    dlg.Destroy()  # Destroy it when finished.


def on_about(self):
    self.config = load_config()

    description = """A Work In Progress tool for our corporate industrialists to
check what can be made from the random salvage items in their hangers.

If you like my work please consider an ISK donation to Elusive One.

All EVE-Online related materials are property of CCP hf."""

    licence = """Eve Online Salvage Solver is released under GNU GPLv3:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."""

    info = wx.adv.AboutDialogInfo()

    # info.SetIcon(wx.Icon('images/wip.png', wx.BITMAP_TYPE_PNG))
    info.SetName('Eve Online Salvage Solver')
    info.SetVersion(self.config.version)
    info.SetDescription(description)
    # info.SetCopyright('(C) 2021 Tim Cumming')
    info.SetWebSite('https://gitlab.com/eluone/WIP')
    info.SetLicence(licence)
    info.AddDeveloper('Tim Cumming aka Elusive One')
    # info.AddDocWriter('')
    # info.AddArtist('')
    # info.AddTranslator('')

    wx.adv.AboutBox(info)

