#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 05/04/21
# Modified: 11/12/21

import sqlite3 as lite
import math
from collections import Counter
from contextlib import closing
from pathlib import Path

import logging
logger = logging.getLogger()


def get_matches(config, mat_id_list):
    logger.debug('Running get_matches...')
    bpo_list = []  # A list of BPOs with materials from the matList
    params = {}  # A dictionary of named parameters to use with SQL statement

    sql_file = Path.cwd() / config.sql_file

    try:
        with closing(lite.connect(sql_file)) as con:
            with closing(con.cursor()) as cur:
                logger.debug(('Matches SQL Query Parameters: %s' % str(mat_id_list)))

                # Get all BPOs that contain the matList materials
                # SQL Joins make output human readable
                # This code adds in a named parameter to the query for each found in mat_id_list
                statement = """SELECT m.materialTypeID, t.typeName, m.quantity, m.typeID, t2.typeName
                            FROM industryActivityMaterials AS m
                            INNER JOIN invTypes AS t ON t.typeID = m.materialTypeID
                            INNER JOIN invTypes AS t2 ON t2.typeID = m.typeID
                            WHERE m.materialTypeID IN ({})
                            AND m.activityID = 1
                            """.format(', '.join(':{}'.format(i) for i in range(len(mat_id_list))))

                params.update({str(i): mat_id for i, mat_id in enumerate(mat_id_list)})

                rows = cur.execute(statement, params).fetchall()

                for row in rows:
                    # materialTypeID, typeName, quantity, typeID, typeName
                    # bpo_list.append([int(row[0]), str(row[1]), int(row[2]), int(row[3]), str(row[4])])

                    # materialTypeID.typeName (Material Name), m.typeID (Blueprint ID)
                    bpo_list.append([str(row[1]), int(row[3])])

    except lite.Error as err:
        error = ('SQL Lite Error: ' + str(err.args[0]) + str(err.args[1:]))  # Error String
        logger.error(error)

    # Generate a Counter object with Blueprint ID/Material Name as keys and matches as values
    out_counter = Counter(x for xs in bpo_list for x in set(xs))

    # Log input materials BPO match count
    for key, value in out_counter.items():
        if type(key) == str:  # Filter out_counter blueprints to only return the material names from keys
            logger.debug(('%s - BPO Matches: %s' % (key, value)))

    return out_counter


def bpo_mats_required_dict(config, bpo_id, bpo_dict):
    """Generates a dictionary of materials required for given bpo ID:
    material name as key of nested dictionary
    {material1: qty, material2: qty...}"""
    mats_req_dict = {}

    if bpo_dict[bpo_id].num_materials_req > 0:
        logger.debug(('%s found in bpo_dict, skipping bpo_mats_required_dict' % str(bpo_id)))
    else:
        logger.debug(('Setting up mats_req_dict for %s...' % str(bpo_id)))

        sql_file = Path.cwd() / config.sql_file

        try:
            with closing(lite.connect(sql_file)) as con:
                with closing(con.cursor()) as cur:
                    # Get all materials required for selected BPO
                    logger.debug(('Materials SQL Query Parameters: %s' % str(bpo_id)))

                    cur.execute("""SELECT materialTypeID, quantity FROM industryActivityMaterials
                                    WHERE typeID = :bpo_id AND activityID = 1""", {'bpo_id': str(bpo_id)})
                    rows = cur.fetchall()

                    for row in rows:
                        # materialTypeID, quantity
                        mats_req_dict[int(row[0])] = int(row[1])

                    bpo_dict[bpo_id].input_materials = mats_req_dict

        except lite.Error as err:
            error = ('SQL Lite Error: ' + str(err.args[0]) + str(err.args[1:]))  # Error String
            logger.critical(error)

    return


def bpo_check_materials(config, bpo_id, mat_qty_dict, bpo_dict):
    """Given Blueprint ID use material quantities dictionary to check against requirements for blueprint"""

    mat_qty_match = 0
    max_poss_runs = 0
    poss_runs_list = []

    # Just a quick check that the data is there
    if bpo_dict[bpo_id].num_materials_req == 0:
        logger.debug(('bpo_check_materials: input_materials NOT found for %s in bpo_dict' % str(bpo_id)))
        bpo_mats_required_dict(config, bpo_id, bpo_dict)

    if bpo_dict[bpo_id].num_materials_req > 0:
        # We have the data in bpo_dict, so continue with checks
        for key, value in mat_qty_dict.items():
            if key in bpo_dict[bpo_id].input_materials:
                poss_runs_list.append(math.floor(value / bpo_dict[bpo_id].input_materials[key]))
                if bpo_dict[bpo_id].input_materials[key] <= value:
                    mat_qty_match += 1

    # If we get no matches from the above checks
    if len(poss_runs_list) == 0:
        poss_runs_list = [0]

    # Smallest value in list of possible runs from material quantity checks
    if len(poss_runs_list) == bpo_dict[bpo_id].num_materials_req:
        max_poss_runs = min(poss_runs_list)

    return mat_qty_match, max_poss_runs


def add_market_data(input_list, bpo_dict, market_data):
    out_list_data = []
    # Add the market data for the Blueprint output products to the out_list columns data
    for row in input_list:
        bpo_product_sell = market_data[bpo_dict[row[0]].output_type_id].sell

        if int(row[3]) > 0:
            bpo_sell_total = row[3] * bpo_product_sell
        else:
            bpo_sell_total = 0

        out_list_data.append([row[0], row[1], row[2], row[3],
                              bpo_product_sell,
                              bpo_sell_total])

    return out_list_data


def sort_for_output(out_list, column):
    """Sort list of lists by given item index
    6: Blueprint Name
    1: Material Types Matched
    2: Material Quantity Match
    3: Possible Production Runs
    4: ISK / Unit
    5: Total"""
    def get_key(item):
        return item[column]
    if column == 6:
        sorted_out_list = sorted(out_list, key=get_key, reverse=False)
    else:
        sorted_out_list = sorted(out_list, key=get_key, reverse=True)

    return sorted_out_list


def unfiltered_bpo_list(config, match_list, mat_qty_dict, bpo_dict):
    out_list = []

    for key, value in match_list.items():
        if type(key) == int:  # Filter out_counter materials to only return the blueprints from keys
            # I have found Blueprint material requirements in the industryActivityMaterials table
            # that do not have a corresponding entry in the industryActivityProducts table
            # so a matching id will not be found in the bpo_dict as there is no product listed.(?)
            if key not in bpo_dict:
                logger.debug(("%s not found in bpo_dict" % str(key)))
            else:
                # Just a quick check that the data is there
                if bpo_dict[key].num_materials_req == 0:
                    logger.debug(('input_materials not found for %s in bpo_dict' % str(key)))
                    bpo_mats_required_dict(config, key, bpo_dict)

                # Check quantities of materials required and the number of total production runs that can be done
                mat_qty_match, max_poss_runs = bpo_check_materials(config, key, mat_qty_dict, bpo_dict)

                # out_list: [0] BPO ID, [1] Material Type Matches, [2] Material Qty Matches, [3] Max Possible Runs
                out_list.append([int(key), int(value), int(mat_qty_match), int(max_poss_runs)])

    return out_list


def apply_user_filters(input_list, bpo_dict, min_runs, num_missing_mats, allow_config_override):
    out_list = []

    def apply_filters():
        for i in range(len(input_list)):
            if min_runs > 0:
                if input_list[i][3] >= min_runs:
                    out_list.append(input_list[i])
            elif num_missing_mats >= 0:
                req_mats = bpo_dict[input_list[i][0]].num_materials_req  # Get number of material types required for BPO
                if (req_mats - num_missing_mats) <= input_list[i][1] <= req_mats:
                    out_list.append(input_list[i])

    if allow_config_override == "Y":
        # If we get no results automatically lower minimum runs until we get a result
        while len(out_list) == 0:
            if min_runs > 0:
                apply_filters()
                min_runs -= 1
            else:
                apply_filters()
                num_missing_mats += 1
    else:
        apply_filters()

    return out_list
