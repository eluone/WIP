#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 29/06/21
# Modified: 01/07/21


class Blueprint(object):
    def __init__(self, bpo_id, type_name, output_type_id, output_type_name, input_materials):
        self.bpo_id = bpo_id  # Blueprint ID as int
        self.type_name = type_name  # Blueprint Name
        self.output_type_id = output_type_id  # Blueprint Product ID
        self.output_type_name = output_type_name  # Blueprint Product Name
        self.input_materials = input_materials  # Dictionary of input materials (ID: Qty)

    # Using Property setters to calculate num_materials when input_materials is set or updated
    @property
    def input_materials(self):
        return self._input_materials

    @input_materials.setter
    def input_materials(self, val):
        self._input_materials = val
        self._num_materials_req = len(val)

    @property
    def num_materials_req(self):
        return self._num_materials_req

# end of class Character


class Item(object):
    def __init__(self, type_id, type_name, market, buy, sell, updated):
        self.type_id = type_id
        self.type_name = type_name
        self.market = market
        self.buy = buy
        self.sell = sell
        self.updated = updated

# end of class Item
