#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 02/04/21
# Modified: 05/10/21

import sqlite3 as lite
from contextlib import closing
from pathlib import Path

from common.classes import Blueprint

import logging
logger = logging.getLogger()


def gen_mat_dict(config):
    """Will generate a dictionary of materials vs their database ids, using material names as keys"""
    logger.debug('Setting up mat_dict...')
    mat_dict = {}  # This is going to be a dictionary of Material Names (Keys) = Material typeID (Values)

    sql_file = Path.cwd() / config.sql_file

    try:
        with closing(lite.connect(sql_file)) as con:
            with closing(con.cursor()) as cur:
                # Get all materials in groupID 754 (Salvaged Materials)
                # SQL Joins make output human readable
                statement = """SELECT typeID, typeName FROM invTypes
                                WHERE groupID = "754" ORDER BY typeName;"""

                logger.debug(('SQL Query: %s' % statement))

                mat_rows = cur.execute(statement).fetchall()

                for row in mat_rows:
                    # typeID, typeName
                    # Build a dictionary of Materials for quick reference later
                    mat_dict[str(row[1])] = int(row[0])

    except lite.Error as err:
        error = ('SQL Lite Error: ' + str(err.args[0]) + str(err.args[1:]))  # Error as String
        logger.critical(error)

    logger.debug('mat_dict Complete...')
    return mat_dict


def gen_bpo_dict(config):
    """Will generate  dictionary of blueprint names vs their database ids, using names as the key"""
    logger.debug('Running bpoList...')
    bpo_dict = {}

    sql_file = Path.cwd() / config.sql_file

    try:
        with closing(lite.connect(sql_file)) as con:
            with closing(con.cursor()) as cur:
                # Get all BPOs and data to setup dictionary
                # Outputs: [0] Blueprint ID, [1] Blueprint Name, [2] Product (Type) ID, [3] Product Name
                statement = """SELECT r.typeID, t.typeName, r.productTypeID, t2.typeName
                                FROM industryActivityProducts AS r
                                INNER JOIN invTypes AS t ON t.typeID = r.typeID
                                INNER JOIN invTypes AS t2 ON t2.typeID = r.productTypeID
                                WHERE activityID is "1"
                                ORDER BY r.typeID;"""

                logger.debug(('SQL Query: %s' % statement))

                rows = cur.execute(statement).fetchall()

                for row in rows:
                    bpo_dict[int(row[0])] = Blueprint(bpo_id=int(row[0]),
                                                      type_name=str(row[1]),
                                                      output_type_id=int(row[2]),
                                                      output_type_name=str(row[3]),
                                                      input_materials={})

    except lite.Error as err:
        error = ('SQL Lite Error: ' + str(err.args[0]) + str(err.args[1:]))  # Error String
        logger.critical(error)

    logger.debug('bpo_dict Complete...')
    return bpo_dict
