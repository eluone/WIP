#!/usr/bin/python
"""WIP"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 02/04/21
# Modified: 15/10/21

import sqlite3 as lite
from collections import Counter

# Not sure which way to get selection list
# 41 Items in "Salvaged Materials" group (754)
# Could be selection or enter values in form?
"""
25601	Fried Interface Circuit
25605	Armor Plates
25590	Contaminated Nanite Compound
25595   Alloyed Tritanium Bar
"""

matList = ["25601", "25605", "25590", "25595"]
params = {}
bpoList = []
res = {}
out = ""

if matList:
    try:
        con = lite.connect('SDE/sde-subset.sqlite')

        with con:
            cur = con.cursor()
            # Get all BPOs that contain the matList materials
            # SQL Joins make output human readable
            statement = """SELECT m.materialTypeID, t.typeName, m.quantity, m.typeID, t2.typeName
                    FROM industryActivityMaterials AS m
                    INNER JOIN invTypes AS t ON m.materialTypeID = t.typeID
                    INNER JOIN invTypes AS t2 ON m.typeID = t2.typeID
                    WHERE m.materialTypeID IN ({})
                    """.format(', '.join(':{}'.format(i) for i in range(len(matList))))

            params.update({str(i): mat_id for i, mat_id in enumerate(matList)})

            print(statement)

            cur.execute(statement, params)
            rows = cur.fetchall()

            for row in rows:
                # materialTypeID, typeName, quantity, typeID, typeName
                # bpoList.append([int(row[0]), str(row[1]), int(row[2]), int(row[3]), str(row[4])])

                # materialTypeID.typeName, typeID.typeName
                bpoList.append([str(row[1]), str(row[4])])

    except lite.Error as err:
        error = ('SQL Lite Error: ' + str(err.args[0]) + str(err.args[1:]))  # Error String
        # onError(error)  # This is for wx later
        print(error)
    finally:
        if con:
            con.close()

# print(bpoList)

# Count occurrences of items (BPOs and Materials) in bpoList (list of lists)
out = Counter(x for xs in bpoList for x in set(xs))
# print(out)

# Some variables for suggestion selections
allMats = len(matList)
eightyPercentMats = round(len(matList)*0.80)

# Print input materials BPO match count
for key, value in out.items():
    if value > allMats:  # Greater than the number of materials inputted
        print(key, value)

# Print all BPOs that have all 3 input materials
for key, value in out.items():
    if eightyPercentMats <= value <= allMats:  # Between 80% & 100% match
        print(key, value)
